package com.example.pubnub.chat

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.example.pubnub.R
import com.example.pubnub.base.BaseFragment
import com.example.pubnub.helper.Constant
import com.example.pubnub.model.Message
import com.google.gson.Gson
import com.pubnub.api.PNConfiguration
import com.pubnub.api.PubNub
import kotlinx.android.synthetic.main.fragment_chat.*


class ChatFragment : BaseFragment<ChatViewModel>() {


    lateinit var mAdapter: ChatAdapter
    private var messagesList: ArrayList<Message> = ArrayList()


    private fun initPubNub() {
        val pnConfiguration = PNConfiguration()
        pnConfiguration.subscribeKey = Constant.SUBSCRIBE_KEY
        pnConfiguration.publishKey = Constant.PUBLISH_KEY
        pnConfiguration.uuid = Gson().toJson(mActivity.sessionUser)
        mViewModel.pubNub = PubNub(pnConfiguration)
    }

    private fun initRV() {
        mAdapter = ChatAdapter(messagesList)
        chatRV.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        chatRV.adapter = mAdapter
    }

    private fun sendMessage(message: String) {
        mViewModel.pubNub.run {
            publish()
                .message(message)
                .channel("DEMO")
                .meta(Gson().toJson(mActivity.sessionUser))
                .async { _, status ->
                    if (!status.isError) {
                        println("Message was published")
                    } else {
                        println("Could not publish")
                    }
                }
        }
    }

    override fun getViewModel(): Class<ChatViewModel> {
        return ChatViewModel::class.java
    }

    override fun start() {
        setChatData()
        initPubNub()
        initRV()
        updateChat()
        mViewModel.pubNub.run {
            addListener(mViewModel.subscribeCallback)
            subscribe()
                .channels(listOf("DEMO")) // subscribe to channels
                .withPresence()
                .execute()
        }
        send.setOnClickListener {
            if (sendMessage.text.toString().isNotEmpty()) {
                sendMessage(sendMessage.text.toString())
                sendMessage.setText("")
            }

        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_chat
    }

    private fun updateChat() {
        mViewModel.isLoading.observe(this, { t ->
            if (t) {
                loading.visibility = View.VISIBLE
            } else {
                loading.visibility = View.GONE
            }
        })

        mViewModel.messagesList1.observe(this,
            { t ->
                mAdapter.updateList(t!!)
                scrollRecyclerViewToBottom(chatRV)
            })
    }

    private fun scrollRecyclerViewToBottom(recyclerView: RecyclerView) {
        val adapter = recyclerView.adapter
        if (adapter != null && adapter.itemCount > 0) {
            recyclerView.scrollToPosition(adapter.itemCount - 1)
        }
    }

    private fun setChatData() {
        chatName.text = "DEMO"
        val generator = ColorGenerator.MATERIAL
        val randomColor = generator.randomColor

        val drawable =
            TextDrawable.builder().buildRoundRect("D", randomColor, 120)
        chatIcon.setImageDrawable(drawable)


    }
}