package com.example.pubnub.chat

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.example.pubnub.helper.MyDiffUtilCallback
import com.example.pubnub.R
import com.example.pubnub.model.Message
import com.example.pubnub.model.User
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList


class ChatAdapter(private var messages: ArrayList<Message>?) :
    RecyclerView.Adapter<ChatAdapter.ViewHolder>() {


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val message: TextView = view.findViewById(R.id.message)
        val userImage: ImageView = view.findViewById(R.id.userImage)
        val userName: TextView = view.findViewById(R.id.userName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.chat_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = messages?.get(position)

        holder.message.text = message?.message
        val generator = ColorGenerator.MATERIAL
        val randomColor = generator.randomColor

        val drawable = TextDrawable.builder()
            .buildRoundRect(
                message?.user?.userName?.substring(0, 1)?.capitalize(Locale.getDefault()),
                randomColor,
                120
            )

        holder.userImage.setImageDrawable(drawable)
        holder.userName.text = message?.user?.userName


    }

    override fun getItemCount() = messages!!.size


    fun updateList(updatedList: ArrayList<Message>) {
        messages = updatedList
        notifyDataSetChanged()
    }

//    fun updateList(updatedList: ArrayList<Message>) {
//        val difUtilResult: DiffUtil.DiffResult =
//            DiffUtil.calculateDiff(MyDiffUtilCallback(messages!!, updatedList))
//        messages = updatedList
//        difUtilResult.dispatchUpdatesTo(this)
//
//    }
}