package com.example.pubnub.chat

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pubnub.model.Message
import com.example.pubnub.model.User
import com.google.gson.Gson
import com.pubnub.api.PubNub
import com.pubnub.api.callbacks.SubscribeCallback
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.objects_api.channel.PNChannelMetadataResult
import com.pubnub.api.models.consumer.objects_api.membership.PNMembershipResult
import com.pubnub.api.models.consumer.objects_api.uuid.PNUUIDMetadataResult
import com.pubnub.api.models.consumer.pubsub.PNMessageResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult
import com.pubnub.api.models.consumer.pubsub.PNSignalResult
import com.pubnub.api.models.consumer.pubsub.files.PNFileEventResult
import com.pubnub.api.models.consumer.pubsub.message_actions.PNMessageActionResult

class ChatViewModel : ViewModel() {

    lateinit var pubNub: PubNub
    var messagesList: ArrayList<Message> = ArrayList()
    var  isLoading:MutableLiveData<Boolean> = MutableLiveData()
    var messagesList1: MutableLiveData<ArrayList<Message>> = MutableLiveData()


    var subscribeCallback: SubscribeCallback = object : SubscribeCallback() {

        override fun status(pubnub: PubNub, status: PNStatus) {
            isLoading.postValue(true)
            pubNub.history().channel("DEMO").includeMeta(true).includeTimetoken(true).async { result, status ->
                result?.messages?.forEach { history ->

                    messagesList.add(
                        Message(
                            history.entry.asString,
                            history.timetoken,
                            Gson().fromJson(history.meta.asString, User::class.java)
                        )
                    )
                }
                isLoading.postValue(false)
                messagesList1.postValue(messagesList)
            }
        }

        override fun message(pubnub: PubNub, message: PNMessageResult) {

            messagesList.add(
                Message(
                    message.message.asString,
                    message.timetoken,
                    Gson().fromJson(message.userMetadata.asString, User::class.java)
                )
            )

            messagesList1.postValue(messagesList)

        }

        override fun presence(pubnub: PubNub, presence: PNPresenceEventResult) {

        }

        override fun signal(pubnub: PubNub, pnSignalResult: PNSignalResult) {


        }

        override fun uuid(pubnub: PubNub, pnUUIDMetadataResult: PNUUIDMetadataResult) {

        }

        override fun channel(pubnub: PubNub, pnChannelMetadataResult: PNChannelMetadataResult) {

        }

        override fun membership(pubnub: PubNub, pnMembershipResult: PNMembershipResult) {

        }

        override fun messageAction(
            pubnub: PubNub,
            pnMessageActionResult: PNMessageActionResult
        ) {

        }

        override fun file(pubnub: PubNub, pnFileEventResult: PNFileEventResult) {

        }
    }

}