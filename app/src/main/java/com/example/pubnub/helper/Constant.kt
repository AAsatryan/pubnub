package com.example.pubnub.helper

object Constant {

    val PUBLISH_KEY = "pub-c-64cbb534-75b1-4208-b286-59a3a75d76c4"
    val SUBSCRIBE_KEY = "sub-c-0a8e7272-0cde-11eb-8b70-9ebeb8f513a7"

    val SHARED_PREFS = "sharedPrefs"
    val KEY = "user"
}
