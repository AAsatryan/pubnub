package com.example.pubnub.helper

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

fun EditText.textChange(text: (String?) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s.toString().isNotEmpty()) {
                text(s.toString())
            } else {
                text(null)
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

    })
}


fun Fragment.add(activity: AppCompatActivity, container: Int) {
    val transaction = activity.supportFragmentManager.beginTransaction()
    transaction.add(container, this)
    transaction.commit()
}
