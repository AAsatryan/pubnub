package com.example.pubnub.helper

import androidx.recyclerview.widget.DiffUtil
import com.example.pubnub.model.Message

open class MyDiffUtilCallback(
    private val oldList: List<Message>,
    private val newList: List<Message>
) :
    DiffUtil.Callback() {


    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].message == newList[newItemPosition].message
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}
