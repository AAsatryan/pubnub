package com.example.pubnub.auth

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.pubnub.R
import com.example.pubnub.model.GenderItem


class SpinnerAdapter(context: Context, objects: ArrayList<GenderItem>) :
    ArrayAdapter<GenderItem>(context, 0, objects) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)!!
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)!!
    }

    private fun initView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var myConvertView: View? = convertView
        if (myConvertView == null) {
            myConvertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, parent, false)
        }
        val imageViewFlag: ImageView = myConvertView!!.findViewById(R.id.icon)
        val textViewName: TextView = myConvertView.findViewById(R.id.text)
        val genderItem: GenderItem? = getItem(position)
        if (genderItem != null) {
            imageViewFlag.setImageResource(genderItem.icon)
            textViewName.text = genderItem.gender
        }
        return myConvertView
    }
}