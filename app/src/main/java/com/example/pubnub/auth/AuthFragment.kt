package com.example.pubnub.auth

import android.view.View
import android.widget.AdapterView
import com.example.pubnub.R
import com.example.pubnub.helper.add
import com.example.pubnub.base.BaseFragment
import com.example.pubnub.chat.ChatFragment
import com.example.pubnub.model.GenderItem
import com.example.pubnub.helper.textChange
import com.example.pubnub.model.User
import kotlinx.android.synthetic.main.fragment_auth.*

class AuthFragment : BaseFragment<AuthViewModel>() {


    lateinit var mAdapter: SpinnerAdapter
    private val chatFragment: ChatFragment = ChatFragment()

    private var mUserName: String? = null
    private var gender: Int = 0


    override fun getViewModel(): Class<AuthViewModel> {
        return AuthViewModel::class.java
    }

    override fun start() {
        addSpinnerData()
        getPageData()
        saveUserDataAndChangeScreen()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_auth
    }

    private fun getPageData() {
        userName.textChange {
            mUserName = it.toString()
            isScreenReady()
        }
    }

    private fun addSpinnerData() {
        val genderList = ArrayList<GenderItem>()
        genderList.add(GenderItem("Select Your gender", R.drawable.ic_select_gender))
        genderList.add(GenderItem("I am Male", R.drawable.ic_male))
        genderList.add(GenderItem("I am Female", R.drawable.ic_female))
        mAdapter = SpinnerAdapter(mActivity, genderList)
        genderSpinner.adapter = mAdapter

        genderSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                gender = p2
                isScreenReady()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }

    }

    private fun isScreenReady() {
        startChat.isEnabled =
            mUserName != null && gender != 0
    }

    private fun saveUserDataAndChangeScreen() {
        startChat.setOnClickListener {
            mActivity.sessionUser = User(mUserName!!,gender)
            mViewModel.saveUserData(mUserName!!, gender, mActivity)
            chatFragment.add(mActivity, R.id.parentLayout)
        }
    }
}