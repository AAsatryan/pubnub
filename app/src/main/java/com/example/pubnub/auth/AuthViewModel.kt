package com.example.pubnub.auth

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import com.example.pubnub.helper.Constant.KEY
import com.example.pubnub.helper.Constant.SHARED_PREFS
import com.example.pubnub.model.User
import com.google.gson.Gson


class AuthViewModel : ViewModel() {


    fun saveUserData(userName: String, gender: Int, context: Context) {
        val user = User(userName, gender)
        val sharedPreferences: SharedPreferences =
            context.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(KEY, Gson().toJson(user))
        editor.apply()
    }
}