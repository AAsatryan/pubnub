package com.example.pubnub.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pubnub.R
import com.example.pubnub.auth.AuthFragment
import com.example.pubnub.chat.ChatFragment
import com.example.pubnub.helper.Constant
import com.example.pubnub.helper.add
import com.example.pubnub.model.User
import com.google.gson.Gson


class MainActivity : AppCompatActivity() {

    lateinit var sessionUser: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.statusBarColor = resources.getColor(R.color.chat_background)
        navigate()

    }


    private fun navigate() {
        val prefs = getSharedPreferences(Constant.SHARED_PREFS, Context.MODE_PRIVATE)
        val user = prefs.getString(Constant.KEY, null)
        if (user != null) {
            sessionUser = Gson().fromJson(user, User::class.java)
            ChatFragment().add(this, R.id.parentLayout)

        } else {
            AuthFragment().add(this, R.id.parentLayout)
        }
    }

}