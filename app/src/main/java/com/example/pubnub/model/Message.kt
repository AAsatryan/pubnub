package com.example.pubnub.model

data class Message(

    val message: String,
    val timeToken:Long?,
    val user: User
)