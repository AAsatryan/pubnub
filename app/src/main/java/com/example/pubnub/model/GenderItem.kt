package com.example.pubnub.model

data class GenderItem(
     var gender: String,
     var icon: Int
)
