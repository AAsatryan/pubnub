package com.example.pubnub.model

data class User(
    val userName: String,
    val gender: Int
)